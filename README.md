# Ican Endpoint

## Atencion
Todas las consultas que se hagan se estan lanzando hacia una nacion real, mucho cuidado. (NB no provee con entornos de prueba)

## Requerimientos

1. node v.10 (puedes suarse nvm para poder tener diferentes versions de node funcionando en la maquina) https://github.com/nvm-sh/nvm
2. acceso a la consola de firebase y a las functiones de firebase
3. firebase cli instalado. 

## Instalacion

1. clonar repositorio
2. acceder al directorio 'functions' que genera automaticamente firebase y hacer un 'npm install'
3. lanzar un 'firebase login' y autenticarse con la misma cuenta que se ha dado de alta en firebase funcionats
4. lanzar un 'firebase emulators:start' para lanzar el emulador local
5. la consola nos devolvera el estado del servicio y nos dara una url para poder acceder tipo: http://localhost:5001/ican-tectonica-fb69c/us-central1/api
6. Al user graphql el enpoint es unico para todas la consultas y es /endpoint que habra que añadir al final de la url propoercionada previamente
7. Nos apacera el graphqli que es una interficie grafica para poder interactuar con el graphql y hacer consultas. 
8. ejemplo consultas:
··1. {
  totalUsers
}
··2. {
	getUsersByTag(tag:"marcel")
}



