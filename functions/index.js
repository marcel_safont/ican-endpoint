const functions = require('firebase-functions');
const express = require('express');
const grTools = require('graphql-tools');
const graphqlHTTP = require('express-graphql');
const graphqlUsers = require('./users.js');
const graphqlPosts = require('./posts.js');
const cors = require('cors');

const app = express();

// app.use(cors({
// 	origin: ['https://bricks.lndo.site', 'https://rise.icanw.org']
// }));

// 18dMiZbGLsF14MuHv5fXeTYjZ0dJYOq2Ui8r0gcJU6eeFBPx9RiEBnRAwtsIAPAEvVVxRbeOPH8eodGvoqyNbSwFziJTNRYVFD9D1c4PvfRjy2LsuSBOaBnU4gZXAlPy

app.use('/controlshift', (req, res) => {
	console.log(req);
	res.send('hola');
})

app.use('/endpoint',graphqlHTTP({
		schema: grTools.mergeSchemas({
			schemas: [graphqlUsers.schema , graphqlPosts.schema]
		}),
		rootValue: Object.assign(graphqlPosts.root, graphqlUsers.root),
		graphiql: true
	})
);

module.exports = {
    api:functions.https.onRequest(app)
}