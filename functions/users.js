const grTools = require('graphql-tools');
const fetch = require('node-fetch');
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

let counter = [
    {'tag': 'test2', 'users': 0}
];

this.schema = new grTools.makeExecutableSchema({
	typeDefs: `
		type User {
            email: String
        }
		type Query {
            totalUsers: Int
            getUsersByTag(tag: String!): Int
            getTotalCurrentUsersByTag(tag: String!): String
            removeCacheUserByTag(tag: String!): String
            addToTotalCurrentUsersByTag(tag: String!): String
        }
	`
});


this.root = {
	totalUsers: async () =>{

        return await fetch('https://ican.nationbuilder.com/api/v1/people/count', {
            method: 'GET',
            mode: 'no-cors',
            headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer 1dc7746c4072b9fc3dc67383482a3ad520e5ba79b751186819fa8e9b21d5cd16'
            },
        })
        .then(response => response.json())
        .then(result => result.people_count)
        
    },
    
    getUsersByTag: async (params) =>{
		async function getUsers(url, users = 0){
                if(myCache.get(params.tag) == undefined){
                    return await fetch(url, {
                        method: 'GET',
                        mode: 'no-cors',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer 1dc7746c4072b9fc3dc67383482a3ad520e5ba79b751186819fa8e9b21d5cd16'
                        },
                    })
                    .then(res => res.json())
                    .then(async items => {
                        if(items.next !== null){
                            let page_users = Object.keys(items.results).length;
                            page_users += await getUsers('https://ican.nationbuilder.com'+items.next, users);  
                            users = page_users;
                        }else{
                            users += Object.keys(items.results).length;
                            
                        }

                        counter.map(p => {
                            if(p.tag == params.tag){
                                p.users = users;
                            }
                            return 'increased';
                        })

                        myCache.set( params.tag, users);
                        return users;
                    });
                }else{
                    return myCache.get(params.tag);
                }
        }
        
        const users = getUsers(`https://ican.nationbuilder.com/api/v1/tags/${params.tag}/people?limit=100`);

        return users;
    },

    removeCacheUserByTag: async(params) => {
        myCache.del(params.tag);
        return 'cache cleared';
    },
    addToTotalCurrentUsersByTag: async (params) => {
        counter.map(p => {
            if(p.tag == params.tag){
                p.tag == p.users++;
            } 
        });
        return 'updated';
        
    },
    getTotalCurrentUsersByTag: async (params) => {
        const result = counter.filter(p => p.tag == params.tag);
        return result[0].users;
    }
};
